resource "aws_efs_file_system" "efs" {
  reference_name = "${var.name}"

  tags {
    Name = "${var.name}"
  }
}

resource "aws_efs_mount_target" "target" {
  count           = "${length(split(",", var.subnets))}"
  file_system_id  = "${aws_efs_file_system.efs.id}"
  subnet_id       = "${element(split(",", var.subnets), count.index)}"
  security_groups = ["${split(",", var.security_groups)}"]
}
