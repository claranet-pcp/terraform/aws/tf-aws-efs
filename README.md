tf-aws-efs
=================

Elastic File System (EFS) - Terraform Module

Usage
-----

```js
module "efs" {
  source          = "git@gogs.bashton.net:Bashton-Terraform-Modules/tf-aws-efs.git"
  name            = "my-efs"
  subnets         = "subnet-1234abcd,subnet-2345bcde,subnet-3456cdef"
  security_groups = "sg-1234abcd,sg-2345bcde,sg-3456cdef"
}
```

Variables
---------

 - `name` - name of the filesystem
 - `subnets` - comma separated list of nat subnet ids
 - `security_groups` - comma separated list of security group ids (Up to 5 groups)

Outputs
-------

 - `efs_id` - file system id
 - `efs_target_ids` - comma separated output of each efs target ids
 - `efs_target_eni_ids` - comma separated output of network interface ids that Amazon EFS created when it created the mount target

