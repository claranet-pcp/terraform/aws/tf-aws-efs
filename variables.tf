variable "name" {
  description = "The name for the EFS"
  type = "string"
  default = "efs"
}

variable "subnets" {
  description = "Comma separated list of subnet ids"
  type = "string"
}

variable "security_groups" {
  description = "Comma separated list of security group ids"
  type = "string"
}
