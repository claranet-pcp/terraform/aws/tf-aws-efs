output "efs_id" {
  value = "${aws_efs_file_system.efs.id}"
}

output "efs_target_ids" {
  value = "${join(",", aws_efs_mount_target.target.*.id)}"
}

output "efs_target_eni_ids" {
  value = "${join(",", aws_efs_mount_target.target.*.network_interface_id)}"
}
